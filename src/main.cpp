#include <Arduino.h>
#include <FastLED.h>

#include "./LedStripHandler/LedStripHandler.h"
#include "./Models/Models.h"

#define NUM_LEDS 400
#define LED_PIN 6
CRGB leds[NUM_LEDS];

Range* WHOLE_RANGE = new Range(50, NUM_LEDS - 20); 
Range* WHOLE_RANGE_REVERSED = new Range(WHOLE_RANGE->endIndex, WHOLE_RANGE->startIndex);

ColorProvider* OFF = new StaticColorProvider(new Color(0,0,0));
ColorProvider* RED = new StaticColorProvider(new Color(255, 0, 0));
ColorProvider* GREEN = new StaticColorProvider(new Color(0, 255, 0));
ColorProvider* PURPLE = new StaticColorProvider(new Color(126, 0, 126));
ColorProvider* BLUE = new StaticColorProvider(new Color(0, 0, 255));
ColorProvider* RED_TO_BLUE = new DynamicColorProvider(RED->getColor(), BLUE->getColor(), WHOLE_RANGE->getLength());
ColorProvider* RED_TO_GREEN = new DynamicColorProvider(RED->getColor(), GREEN->getColor(), WHOLE_RANGE->getLength());

ColorProvider* RED_GREEN_PROVIDER;
ColorProvider* GREEN_RED_PROVIDER;

Color* RED_GREEN_ARRAY[2];
Color* GREEN_RED_ARRAY[2];

 
//function declarations
void lightLED(Color* color, int index);
void setColorLED(Color* color, int index);
void updateLEDs();
void clear(Range* range);
void sequence(Range* range, ColorProvider* colorProvider, int delayMillis, boolean fill=true);
void bounce(Range* range, ColorProvider* colorProvider, int delayMillis, boolean fill=false);
void collide(Range* range, Color* color1, Color* color2, Color* color3, int delayMillis, int fill=false);
void alternate(Range* range, ColorProvider* colorProvider1, ColorProvider* colorProvider2, int delayMillis, int numSwaps);
void lightRange(Range* range, ColorProvider* colorProvider);

void setup() {
  Serial.begin(9600);
  FastLED.addLeds<WS2811, LED_PIN, GRB>(leds, NUM_LEDS);

  RED_GREEN_ARRAY[0] = RED->getColor();
  RED_GREEN_ARRAY[1] = GREEN->getColor();

  GREEN_RED_ARRAY[0] = GREEN->getColor();
  GREEN_RED_ARRAY[1] = RED->getColor();

  RED_GREEN_PROVIDER = new ArrayColorProvider(RED_GREEN_ARRAY, 2);
  GREEN_RED_PROVIDER = new ArrayColorProvider(GREEN_RED_ARRAY, 2);
}


void loop() {
  alternate(WHOLE_RANGE, RED_GREEN_PROVIDER, GREEN_RED_PROVIDER, 500, 10);
  clear(WHOLE_RANGE);

  sequence(WHOLE_RANGE, RED, 10, true);
  sequence(WHOLE_RANGE_REVERSED, GREEN, 10, true);
  clear(WHOLE_RANGE);

  bounce(WHOLE_RANGE, BLUE, 10, false);
  clear(WHOLE_RANGE);

  collide(WHOLE_RANGE, RED->getColor(), BLUE->getColor(), GREEN->getColor(), 10, true);
  clear(WHOLE_RANGE);
}

void lightLED(Color* color, int index){
  if(index < 0 || index >= NUM_LEDS || color == nullptr) return;
    leds[index] = CRGB(color->g, color->r, color->b);
    FastLED.show();
}

void setColorLED(Color* color, int index){
  leds[index] = CRGB(color->g, color->r, color->b);
}

void updateLEDs(){
    FastLED.show();
}

void clear(Range* range){
  lightRange(range, OFF);
}

void sequence(Range* range, ColorProvider* colorProvider, int delayMillis, boolean fill=true){
  int delta = 1;
  int lowerBound = range->startIndex;
  int upperBound = range->endIndex;

  if(range->startIndex > range->endIndex){
    delta = -1;
    lowerBound = range->endIndex;
    upperBound = range->startIndex;
  }

  int ptr = range->startIndex;
  while(ptr >= lowerBound && ptr <= upperBound){
    lightLED(colorProvider->getColor(), ptr);
    if(!fill){
        lightLED(OFF->getColor(), ptr - delta);
    }
    if (delayMillis !=0 ){
      delay(delayMillis);
    }

    ptr = ptr + delta;
  }

  if(!fill){
    lightLED(OFF->getColor(), range->endIndex);
  }
}

void bounce(Range* range, ColorProvider* colorProvider, int delayMillis, boolean fill=false){
  sequence(range, colorProvider, delayMillis, fill);
  Range* reverseRange = nullptr;
  if(fill){
    reverseRange = new Range(range->endIndex, range->startIndex);
  }else{
    reverseRange = new Range(range->endIndex, range->startIndex);
  }

  sequence(reverseRange, colorProvider, delayMillis, false);
  delete reverseRange;
}

void collide(Range* range, Color* color1, Color* color2, Color* color3, int delayMillis, int fill){
    int startPtr = range->startIndex;
    int endPtr = range->endIndex;

    while(startPtr < endPtr - 1){
      lightLED(color1, startPtr);
      lightLED(color2, endPtr);

      if(!fill){
        lightLED(OFF->getColor(), startPtr - 1);
        lightLED(OFF->getColor(), endPtr + 1);
      }


      delay(delayMillis);
      startPtr++;
      endPtr--;
    }

    if(!fill){
      lightLED(OFF->getColor(), startPtr - 1);
      lightLED(OFF->getColor(), endPtr + 1);
    }

    while(startPtr >= range->startIndex){
      lightLED(color3, startPtr);
      lightLED(color3, endPtr);

      if(startPtr != endPtr - 1 && !fill){
        lightLED(OFF->getColor(), startPtr + 1);
        lightLED(OFF->getColor(), endPtr - 1);
      }

      delay(delayMillis);
      startPtr--;
      endPtr++;
    }

    if(!fill){
      lightLED(OFF->getColor(), startPtr + 1);
      lightLED(OFF->getColor(), endPtr - 1);
    }

    delay(delayMillis);
}

void alternate(Range* range, ColorProvider* colorProvider1, ColorProvider* colorProvider2, int delayMillis, int numSwaps){
  for(int i = 0; i < numSwaps; i++){
    lightRange(range, colorProvider1);
    delay(delayMillis);
    lightRange(range, colorProvider2);
    delay(delayMillis);
  }
}

void lightRange(Range* range, ColorProvider* colorProvider){
  if(range->startIndex < range->endIndex){
    for(int i = range->startIndex; i < range->endIndex; i++){
      setColorLED(colorProvider->getColor(), i);
    }
  }else{
    for(int i = range->endIndex; i >= range->startIndex; i--){
      setColorLED(colorProvider->getColor(), i);
    }
  }

  updateLEDs();
}