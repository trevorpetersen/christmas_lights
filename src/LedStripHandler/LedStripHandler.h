#include <FastLED.h>

class LedStripHandler{
    private:
    int index;
    public:
    virtual void handle(CRGB leds[], int ledLength) = 0;
};

class LedStripHandlerSingleLed : public LedStripHandler{
    private:
    int ledIndex;
    public:
    LedStripHandlerSingleLed(int i);
    void handle(CRGB leds[], int ledLength);
};