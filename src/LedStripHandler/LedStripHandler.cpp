#include "LedStripHandler.h"


LedStripHandlerSingleLed::LedStripHandlerSingleLed(int i){
        ledIndex = i;
}
void LedStripHandlerSingleLed::handle(CRGB leds[], int ledLength){
        leds[ledIndex] = CRGB(0,0,255);
}