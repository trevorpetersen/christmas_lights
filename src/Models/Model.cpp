#include "Models.h"

Color::Color(int r, int g, int b): r(r), g(g), b(b){}

StaticColorProvider::StaticColorProvider(Color* color): color(color){}
Color* StaticColorProvider::getColor(){return color;};

DynamicColorProvider::DynamicColorProvider(Color* color1, Color* color2, int numSteps): color1(color1), color2(color2), numSteps(numSteps){currentStep = 0;}
Color* DynamicColorProvider::getColor(){
    if(numSteps < 2) return color1;

    int rSeg = (color2->r - color1->r) / (numSteps - 1);
    int gSeg = (color2->g - color1->g) / (numSteps - 1);
    int bSeg = (color2->b - color1->b) / (numSteps - 1);

    delete currentColor;
    currentColor = new Color(color1-> r + (rSeg * currentStep), color1->g + (gSeg * currentStep), color1->b + (bSeg * currentStep));
    currentStep = (currentStep + 1) % numSteps;
    return currentColor;
};

ArrayColorProvider::ArrayColorProvider(Color** colors, int numColors): colors(colors), numColors(numColors){currentIndex = 0;}
Color* ArrayColorProvider::getColor(){
    Color* currentColor = colors[currentIndex];
    currentIndex = (currentIndex + 1) % numColors;

    return currentColor;
}

Range::Range(int startIndex, int endIndex): startIndex(startIndex), endIndex(endIndex){}
int Range::getLength(){
    if(startIndex > endIndex){
        return startIndex - endIndex + 1;
    }else{
        return endIndex - startIndex + 1;
    }
}