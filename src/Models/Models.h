class Color{
    public:
    int r, g, b;
    Color(int r, int g, int b);
};

class ColorProvider{
    public:
    virtual Color* getColor() = 0;
};

class StaticColorProvider: public ColorProvider{
    private:
    Color* color;
    public:
    StaticColorProvider(Color* color);
    Color* getColor();
};

class DynamicColorProvider: public ColorProvider{
    private:
    Color* color1;
    Color* color2;
    Color* currentColor;

    int numSteps;
    int currentStep;
    public:
    DynamicColorProvider(Color* color1, Color* Color2, int numSteps);
    Color* getColor();
};

class ArrayColorProvider: public ColorProvider{
    private:
    Color** colors;
    int numColors;
    int currentIndex;
    
    public:
    ArrayColorProvider(Color** colors, int numColors);
    Color* getColor();
};

class Range{
    public:
    int startIndex, endIndex;
    Range(int startIndex, int endIndex);
    int getLength();
};